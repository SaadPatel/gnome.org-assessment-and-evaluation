GNOME.ORG ASSESSMENT: 
Just after landing, when full page loads,
Similar gif like homepage vid1.mkv should play to show apps of gnome
As we scroll… The page should play wonderfully similar to  https://www.apple.com/in/macbook-pro-16/: The scroll view UI is awesome

Here instead of laptop view, we can add about our apps like gnome-games, gnome-calendar, gnome-sound-recorder, gnome-extensions….  Small animations in scroll view on home page
When we land on a webpage the animations or the UI or the video which will play automatically, impresses the user a lot. Similar to https://www.tectonica.co/ we can also make blocks and the gnome video animation and play on the main web page.

Display project section:
We can make separate HTML pages for each project and will be represented in each scroll.
For the background, we can also have multiple gnome logos. And when we touch the project slide it will get full screen removing the background. Similar to https://andre.work/#/index. 
Full-screen view of projects similar to https://andre.work/#/roku


For news section,
We can have two vertical parts of the screen, The left part contains Heading, logo, and image. The right part contains a detailed description of the news, related links, guides and person to reach out. Similar to about section of this page, http://melaniedaveid.com/. 



